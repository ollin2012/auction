import { Injectable } from '@angular/core';

@Injectable()
export class ProductService {

  private products: Product[] = [
    new Product(1, "第一个商品", 1.99, 3.5, "这是一个商品，是我在学习Angular2时的创建的案例01", ["电子产品", "硬件设备"]),
    new Product(2, "第二个商品", 2.99, 2.5, "这是二个商品，是我在学习Angular2时的创建的案例02", ["生活用品", "洗涤用品"]),
    new Product(3, "第三个商品", 3.99, 1.5, "这是三个商品，是我在学习Angular2时的创建的案例03", ["生活用品", "厨房工具"]),
    new Product(4, "第四个商品", 4.99, 0.5, "这是四个商品，是我在学习Angular2时的创建的案例04", ["食品", "即食食品"]),
    new Product(5, "第五个商品", 15.99, 3.5, "这是五个商品，是我在学习Angular2时的创建的案例05", ["办公用品", "文具"]),
    new Product(6, "第六个商品", 16.99, 3.5, "这是六个商品，是我在学习Angular2时的创建的案例06", ["军事装备", "枪械"])
  ]
  private comments:Comment[] = [
    new Comment(1,1,"2017-02-02 21:22:22","张三",3,"东西不错"),
    new Comment(2,1,"2017-03-03 22:22:22","李四",4,"东西还好"),
    new Comment(3,1,"2017-04-04 23:22:22","王五",2,"东西一般"),
    new Comment(4,2,"2017-05-05 20:22:22","赵六",4,"东西很好")
    
  ];
  constructor() { }

  getProducts() : Product[]{
    return this.products;
  }

  getProduct(id: number): Product {
    return this.products.find((product) => product.id == id)
  }

  getCommentsForProductId(id: number) : Comment[] {
    return this.comments.filter((comment: Comment) => comment.productId == id)
  }
}
export class Product {
  constructor(
    public id: number,
    public title: string,
    public price: number,
    public rating: number,
    public desc: string,
    public categories: Array<string>
  ) {

  }
}
export class Comment {

  constructor(public id: number,
              public productId: number,
              public timestamp: string,
              public user: string,
              public rating: number,
              public content: string
            ) {

  }

}